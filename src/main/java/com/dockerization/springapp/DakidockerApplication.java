package com.dockerization.springapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DakidockerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DakidockerApplication.class, args);
    }

}
