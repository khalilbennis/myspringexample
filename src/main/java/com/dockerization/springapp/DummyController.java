package com.dockerization.springapp;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DummyController {
    @Autowired
    ProductRepo productRepo;
    @RequestMapping
    public ResponseEntity<List<Product>> getProducts(){
    return new ResponseEntity<>(productRepo.findAll(), HttpStatus.ACCEPTED);
    }

    @RequestMapping
    public ResponseEntity<Product> addProduct(Product product){
        productRepo.save(product);
        return new ResponseEntity<>(null, HttpStatus.ACCEPTED);
    }

}
